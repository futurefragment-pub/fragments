import json
import csv
import os
from pathlib import Path
from argparse import ArgumentParser

class Convert():
    def __init__(self, folder_name, output_filename="output.csv", deep_search=False):
        self.folder_name = Path(str(folder_name))
        self.first_row = ["description", "filename", "x_min", "y_min", "x_max", "y_max"]
        self.output_csv_file = self.folder_name / output_filename
        self.json_files = self.get_json_files(deep_search)
        self.convert_to_csv()

    def get_json_files(self, deep_search):
        """
        This function serves to list all json files in a directory, if 'deep_search' then
        this function go further into the directory structure
        :param deep_search: boolean input to state if you wish to go deep into a directory
        :return: dictionary of json files, mapped to their corresponding dir
        """
        list_path = os.listdir(str(self.folder_name))
        json_files = dict()
        if not deep_search:
            json_files[Path(self.folder_name)] = [Path(i) for i in list_path if i.endswith(".json")]
        else:
            for subdir, dirs, files in os.walk(str(self.folder_name)):
                json_files[Path(subdir)] = ([Path(i) for i in files if i.endswith(".json")])
        return json_files

    def convert_to_csv(self):
        """
        This function converts all the json files, into one csv file.
        :param: None
        :return: None
        """
        with open(str(self.output_csv_file), 'w') as output_file:
            csv_writer = csv.writer(output_file)
            csv_writer.writerow(self.first_row)

            for json_files_dir, json_files in self.json_files.items():
                for json_file in json_files:
                    with open(str(json_files_dir / json_file)) as jf:
                        json_data = json.load(jf)
                        for x in json_data["shapes"]:
                            csv_writer.writerow([x["label"],
                                                 self.folder_name / json_data["imagePath"],
                                                 x["points"][0][0],
                                                 x["points"][0][1],
                                                 x["points"][2][0],
                                                 x["points"][2][1]])


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument('-f', '--folder', type=str)
    parser.add_argument('-d', '--deep', default=False, type=bool)
    #parser.add_argument('-o', '--output', default='output.csv', type=bool)

    args = vars(parser.parse_args())
    folder_path = args['folder']
    d_search = args['deep']
    #out_filename = args['output']

    cv = Convert(folder_path, deep_search=d_search)
#&



