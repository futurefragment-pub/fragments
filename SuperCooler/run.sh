#!/bin/bash
## Set mydir to the directory containing the script
## The ${var%pattern} format will remove the shortest match of
## pattern from the end of the string. Here, it will remove the
## script's name,. leaving only the directory. 
working_dir="${0%/*}"

source "$working_dir"/venv/bin/activate && python3 gui.py

