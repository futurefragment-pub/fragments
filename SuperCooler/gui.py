import sys
from PyQt5.QtWidgets import QApplication, QWidget, QPushButton
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import pyqtSlot


#!/usr/bin/env python
from subprocess import Popen, PIPE
from subprocess import call

import os


class App(QWidget):

    def __init__(self):
        super().__init__()
        self.title = 'SuperCooler'
        self.left = 10
        self.top = 10
        self.width = 320
        self.height = 200

        self.on = False

        self.initUI()

    def initUI(self):
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)


        button = QPushButton('SuperCool On/Off', self)
        button.setToolTip('This is an example button')
        button.move(100, 70)
        button.clicked.connect(self.on_click)

        self.show()

    @pyqtSlot()
    def on_click(self):

        self.on = not self.on
        sudo_password = 'yourpasswordhere'

        cmd_start = './supercool start'
        cmd_stop = './supercool stop'

        if self.on:
            print('Supercool On')
            #os.popen("sudo -S %s"%('./supercool start'), 'w').write(sudo_password)
            call('echo {} | sudo -S {}'.format(sudo_password, cmd_start), shell=True)

        else:
            print('Supercool Off')
            #os.popen("sudo -S %s"%('./supercool stop'), 'w').write(sudo_password)
            call('echo {} | sudo -S {}'.format(sudo_password, cmd_stop), shell=True)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())
